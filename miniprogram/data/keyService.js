/*******************钥匙数据操作***************************/
const util = require("../utils/util.js");
const cryptoJs = require("../utils/cryptoJs.js");

/*******************数据操作***************************/
//合并-暂未使用
const mergeData = (jsonData1, jsonData2) => {

  /*****合并category********/
  const categoryArry = [];
  const categoryArry1 = jsonData1.category;
  const categoryArry2 = jsonData2.category;

  for (var i = 0; i < categoryArry1.length; i++) {
    const category1 = categoryArry1[i];
    const category2 = first(categoryArry2, (item) => item.keyID = category1.keyID);

    if (!category2 || category1.time > category2.time) {
      //data1新增的数据或者data1的时间大于data2的时间
      categoryArry.push(category1);
    } else {
      categoryArry.push(category2);
    }
  }
  //data2中新增的
  for (var i = 0; i < categoryArry2.length; i++) {
    const category2 = categoryArry2[i];
    const category1 = first(categoryArry1, (item) => item.keyID = category2.keyID);

    if (!category1) {
      categoryArry.push(category2);
    }
  }

  /*******合并key********/
  const keyArry = [];
  const keyArry1 = jsonData1.key;
  const keyArry2 = jsonData2.key;

  //将data1中的数据更新到keyArry
  for (var i = 0; i < keyArry1.length; i++) {
    const key1 = keyArry1[i];
    const key2 = first(keyArry2, (item) => item.keyID = key1.keyID);

    if (!key2 || key1.time > key2.time) {
      //data1新增的数据或者data1的时间大于data2的时间
      keyArry.push(key1);
    } else {
      keyArry.push(key2);
    }
  }
  //data2中新增的
  for (var i = 0; i < keyArry2.length; i++) {
    const key2 = keyArry2[i];
    const key1 = first(keyArry1, (item) => item.keyID = key2.keyID);

    if (!key1) {
      keyArry.push(key2);
    }
  }

  const isData1 = data1.time > data2.time;
  const isData1Set = data1.set.time > data2.set.time;

  return retData = {
    "version": isData1 ? data1.version : data2.version,
    "openID": isData1 ? data1.openID : data2.openID,
    "unionID": isData1 ? data1.unionID : data2.unionID,
    "time": isData1 ? data1.time : data2.time,
    "set": isData1Set ? data1.set : data2.set,
    "category": categoryArry,
    "key": keyArry
  }
}
//解密
const deJsonData = (jsonData, password = undefined) => {
  let retJsonData = util.copyObject(jsonData);
  if (password == undefined || password == '') {
    return retJsonData;
  }

  const categoryArry = retJsonData.category
  for (var i = 0; i < categoryArry.length; i++) {
    categoryArry[i].name = cryptoJs.AesDecrypt(categoryArry[i].name, password);
  }

  const keyArry = retJsonData.key
  for (var i = 0; i < keyArry.length; i++) {
    keyArry[i].categoryName = cryptoJs.AesDecrypt(keyArry[i].categoryName, password);
    keyArry[i].keyName = cryptoJs.AesDecrypt(keyArry[i].keyName, password);
    keyArry[i].keyAccount = cryptoJs.AesDecrypt(keyArry[i].keyAccount, password);
    keyArry[i].keyPwd = cryptoJs.AesDecrypt(keyArry[i].keyPwd, password);
    keyArry[i].keyPwd2 = cryptoJs.AesDecrypt(keyArry[i].keyPwd2, password);
    keyArry[i].keyRemark = cryptoJs.AesDecrypt(keyArry[i].keyRemark, password);
    keyArry[i].keyUrl = cryptoJs.AesDecrypt(keyArry[i].keyUrl, password);
  }
  return retJsonData
}
//加密
const enJsonData = (jsonData, password) => {
  let retJsonData = util.copyObject(jsonData);
  const categoryArry = retJsonData.category
  for (var i = 0; i < categoryArry.length; i++) {
    categoryArry[i].name = cryptoJs.AesEncrypt(categoryArry[i].name, password);
  }

  const keyArry = retJsonData.key
  for (var i = 0; i < keyArry.length; i++) {
    keyArry[i].categoryName = cryptoJs.AesEncrypt(keyArry[i].categoryName, password);
    keyArry[i].keyName = cryptoJs.AesEncrypt(keyArry[i].keyName, password);
    keyArry[i].keyAccount = cryptoJs.AesEncrypt(keyArry[i].keyAccount, password);
    keyArry[i].keyPwd = cryptoJs.AesEncrypt(keyArry[i].keyPwd, password);
    keyArry[i].keyPwd2 = cryptoJs.AesEncrypt(keyArry[i].keyPwd2, password);
    keyArry[i].keyRemark = cryptoJs.AesEncrypt(keyArry[i].keyRemark, password);
    keyArry[i].keyUrl = cryptoJs.AesEncrypt(keyArry[i].keyUrl, password);
  }
  return retJsonData
}
//解密key
const deKeyData = (key, password) => {
  const dekey = util.copyObject(key);
  if (password == undefined || password == '') {
    return dekey;
  }

  dekey.categoryName = cryptoJs.AesDecrypt(key.categoryName, password);
  dekey.keyName = cryptoJs.AesDecrypt(key.keyName, password);
  dekey.keyAccount = cryptoJs.AesDecrypt(key.keyAccount, password);
  dekey.keyPwd = cryptoJs.AesDecrypt(key.keyPwd, password);
  dekey.keyPwd2 = cryptoJs.AesDecrypt(key.keyPwd2, password);
  dekey.keyRemark = cryptoJs.AesDecrypt(key.keyRemark, password);
  dekey.keyUrl = cryptoJs.AesDecrypt(key.keyUrl, password);
  return dekey;
}
//解密分类集合
const deCategoryArry = (categoryArry, password) => {
  let retcategoryArry = util.copyObject(categoryArry);
  if (password == undefined || password == '') {
    return retcategoryArry;
  }

  for (var i = 0; i < retcategoryArry.length; i++) {
    retcategoryArry[i].name = cryptoJs.AesDecrypt(retcategoryArry[i].name, password);
  }
  return retcategoryArry;
};


//获取解密后key的分组
const getkeyGroup = (keyArry, password = "") => {
  if (keyArry == undefined || keyArry == '' || keyArry.length == 0) {
    return [];
  }

  const currKeyArry = util.copyObject(keyArry).sort((a, b) => {
    if (a.categorySort != b.categorySort)
      return a.categorySort - b.categorySort;
    else
      return a.sort - b.sort;
  });

  const groupArry = [];
  let tempGroup = {};
  for (var i = 0; i < currKeyArry.length; i++) {
    const currKey = currKeyArry[i];
    if (password != undefined && password != '') {
      currKey.categoryName = cryptoJs.AesDecrypt(currKey.categoryName, password);
      currKey.keyName = cryptoJs.AesDecrypt(currKey.keyName, password);
      currKey.keyAccount = cryptoJs.AesDecrypt(currKey.keyAccount, password)
    }
    if (tempGroup.categoryID == currKey.categoryID) {
      tempGroup.keyArry.push(currKey);
      continue;
    }
    if (tempGroup.categoryID) {
      tempGroup.keyArry = tempGroup.keyArry.sort((a, b) => {
        return a.sort - b.sort;
      });
      groupArry.push(tempGroup);
      tempGroup = {};
    }
    tempGroup.categoryID = currKey.categoryID;
    tempGroup.categoryName = currKey.categoryName;
    tempGroup.categoryColor = currKey.categoryColor;
    tempGroup.categorySort = currKey.categorySort;

    tempGroup.keyArry = [currKey]
  }
  if (tempGroup.categoryID) {
    tempGroup.keyArry = tempGroup.keyArry.sort((a, b) => {
      return a.sort - b.sort;
    });
    groupArry.push(tempGroup);
    tempGroup = {};
  }
  return groupArry;
}
//修改Key
const editKey = (keyModel, jsonData) => {
  if (!keyModel)
    return false;

  const nowTime = new Date().getTime();
  keyModel.time = nowTime;
  let keyArry = jsonData.key;
  //添加
  if (!keyModel.keyID || keyModel.keyID <= 0) {
    keyModel.keyID = getMaxKeyID(keyArry) + 1;
    keyModel.sort = getMaxSort(keyModel.categoryID, keyArry) + 1;

    keyArry.push(keyModel);
    jsonData.key = keyArry.sort((a, b) => {
      return a.sort - b.sort
    });
    return true;
  }
  //修改
  for (var i = 0; i < keyArry.length; i++) {
    if (keyArry[i].keyID != keyModel.keyID)
      continue;

    keyArry[i] = keyModel;
    jsonData.key = keyArry.sort((a, b) => {
      return a.sort - b.sort
    });
    jsonData.time = nowTime;
    return true;
  }
  return false;
}
const getMaxKeyID = (keyArry) => {
  let maxID = 0;
  for (let i = 0; i < keyArry.length; i++) {
    if (keyArry[i].keyID > maxID)
      maxID = keyArry[i].keyID;
  }
  return maxID;
}
const getMaxSort = (categoryID, keyArry) => {
  let maxSort = 0;
  for (let i = 0; i < keyArry.length; i++) {
    const currKey = keyArry[i];
    if (currKey.categoryID == categoryID && currKey.sort > maxSort)
      maxSort = currKey.sort;
  }
  return maxSort;
}

//修改分类列表
const editCategory = (category, jsonData) => {
  if (!category || !category.id)
    return false;

  const categoryArry = jsonData.category;

  for (let i = 0; i < categoryArry.length; i++) {
    if (categoryArry[i].id != category.id)
      continue;

    categoryArry[i] = category;
    //更新key里的分类值
    let keyArry = jsonData.key;
    for (let j = 0; j < keyArry.length; j++) {
      if (keyArry[j].categoryID != category.id)
        continue;

      keyArry[j].categoryName = category.name;
      keyArry[j].categoryColor = category.color;
      keyArry[j].categorySort = category.sort;
    }
    //重新排序
    jsonData.key = keyArry.sort((a, b) => {
      return a.categorySort - b.categorySort
    });
    jsonData.category = categoryArry.sort((a, b) => {
      return a.sort - b.sort
    });
    return true;
  }
  return false;
}

//暴露接口
module.exports = {
  deJsonData: deJsonData,
  enJsonData: enJsonData,

  deKeyData: deKeyData,
  deCategoryArry: deCategoryArry,

  getkeyGroup: getkeyGroup,
  editKey: editKey,
  editCategory: editCategory
};