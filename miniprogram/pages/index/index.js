const util = require('../../utils/util.js');
const cryptoJs = require('../../utils/cryptoJs.js');
const keyService = require('../../data/keyService.js');
const userInfoService = require('../../data/userInfoService.js');
const globalData = getApp().globalData;

Page({
  data: {
    statusHeight: globalData.navBarInfo.statusHeight,
    navHeight: globalData.navBarInfo.navHeight,
    scrollTop: 0,

    isShowKeyList: false,
    isShowWelcome: false,
    butDisabled: true,
    isDemoData: false,
    isCloudLoad: false,
    localJsonData: {},

    isPassword: false,
    keyAddColor: '#4cb050',
    keyGroupArry: []
  },
  //用户设置中心
  centerBind(event) {
    util.navigateTo('../center/center');
  },
  //头部标题
  titleBind(event) {
    if (globalData.password == '') {
      util.showToast('必须输入主密码才能操作！');
      return;
    }
    if (!globalData.userInfo.isCloudSyn) {
      util.showToast('未开启云同步！');
      return;
    }
    let localJsonData = util.getStorageData(globalData.localJsonName);
    util.setCloudDbData(globalData.cloudJsonName, localJsonData).then(res => {
      util.showToast('云同步成功！');
    }).catch(err => {
      console.error(err);

      util.showToast('云同步失败,请重试!');
    });
  },
  //添加
  addKeyBind(event) {
    if (globalData.password == '') {
      util.navigateTo('../verifypwd/verifypwd?isDemoData=' + this.data.isDemoData + '&isCloudLoad=' + this.data.isCloudLoad);
    } else {
      util.navigateTo('../edit/edit?type=1');
    }
  },
  //查看
  viewKeyBind(event) {
    const keyID = event.currentTarget.dataset.keyid;
    util.navigateTo('../view/view?keyID=' + keyID);
  },
  //页面滚动执行方式
  viewScrollBind(event) {
    let scrollTop = event.detail.scrollTop;
    this.setData({
      scrollTop: scrollTop
    })
  },
  //授权登陆
  userInfoBind: function() {
    if (globalData.isLogin) {
      userInfoService.getWxUserInfo(globalData).then(res => {
        if (res == undefined) { //取消授权
          return;
        }
        this.initLocalData();
      });
    } else {
      util.showToast('登录失败，请重试！');
    }
  },
  //开始使用
  startUseBind: function() {
    this.initLocalData();
  },
  //使用指南
  guideBind: function() {
    util.navigateTo('../center/childpages/guide/guide');
  },
  //常见问题
  qaBind: function() {
    util.navigateTo('../center/childpages/problem/problem');
  },

  //本地第一次使用
  locadCloudData: function() {
    wx.setNavigationBarColor({
      frontColor: '#000000',
      backgroundColor: '#000000'
    });

    let localJsonData = {};
    userInfoService.mergeCloudLocalData(globalData).then(res => {
      const isDemoData = res.isDemoData;
      if (res.hasRetJsonData) {
        localJsonData = res.retJsonData
      }

      this.setData({
        isShowWelcome: true,
        butDisabled: false,
        isDemoData: isDemoData,
        isCloudLoad: true,
        localJsonData: localJsonData
      });
    }).catch(err => {
      console.error(err);
      this.setData({
        isShowWelcome: true,
        butDisabled: true
      });
      util.showToast("SKey初始化失败！");
    });
  },
  //初始化本数据
  initLocalData: function() {
    const localJsonData = this.data.localJsonData;

    globalData.isLocalFirstUse = false;

    globalData.userInfo.enPassword = localJsonData.userInfo.enPassword;
    globalData.userInfo.isCloudSyn = localJsonData.userInfo.isCloudSyn;

    localJsonData.userInfo = globalData.userInfo;

    util.setStorageData(globalData.localJsonName, localJsonData);
    util.setStorageData(globalData.localConfigName, globalData);

    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#ffffff'
    })
    util.navigateTo('../verifypwd/verifypwd?isDemoData=' + this.data.isDemoData + '&isCloudLoad=' + this.data.isCloudLoad);
    this.setData({
      isShowKeyList: true,
      isShowWelcome: false,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    //获取用户公开信息
    if (globalData.hasInfo) {
      userInfoService.getWxUserInfo(globalData).catch(err => {
        console.log(err)
      });
    }

    //本地第一次使用
    if (globalData.isLocalFirstUse) {
      this.locadCloudData();
      return;
    }

    //本地没有开启云同步
    if (!globalData.userInfo.isCloudSyn) {
      this.setData({
        isShowKeyList: true
      });
      util.navigateTo('../verifypwd/verifypwd');
      return;
    }

    //本都开启了云同步
    userInfoService.getWxScopeUserInfo().then(res => {
      if (res) { //开启了授权
        this.setData({
          isShowKeyList: true
        });
        util.navigateTo('../verifypwd/verifypwd');
        return;
      }
      this.locadCloudData();
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function(options) {
    if (!this.data.isShowKeyList) //不显示钥匙列表
      return;

    const localJsonData = util.getStorageData(globalData.localJsonName);
    if (localJsonData == '')
      return;

    const keyGroupArry = keyService.getkeyGroup(localJsonData.key, globalData.password);

    let keyAddColor = this.data.keyAddColor;
    if (keyGroupArry.length > 0)
      keyAddColor = keyGroupArry[0].categoryColor;

    this.setData({
      isPassword: globalData.password != '',
      keyAddColor: keyAddColor,
      keyGroupArry: keyGroupArry,
    })
  }
});