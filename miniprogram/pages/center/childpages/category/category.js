const util = require('../../../../utils/util.js');
const cryptoJs = require('../../../../utils/cryptoJs.js');
const keyService = require('../../../../data/keyService.js');
const globalData = getApp().globalData;

Page({
  data: {
    editVisible: false,
    autofocus: false,
    inputValue: '',
    currCategory: {},
    catArry: []
  },
  catClickBind: function(event) {
    const category = event.currentTarget.dataset.catitem;
    this.setData({
      autofocus: true,
      editVisible: true,
      inputValue: category.name,
      currCategory: category
    });
  },
  //输入框
  inputInputBind: function(event) {
    this.setData({
      inputValue: event.detail.value
    })
  },
  //取消
  cancelBind: function(event) {
    this.setData({
      editVisible: false
    })
  },
  //确定
  okBind: function(event) {
    if (globalData.password == '') {
      util.showToast('必须输入主密码才能操作！');
      return;
    }
    if (this.data.inputValue.trim() == '') {
      util.showToast('请输入分类名！');
      return;
    }
    const currCategory = this.data.currCategory;
    currCategory.name = cryptoJs.AesEncrypt(this.data.inputValue, globalData.password);

    let localJsonData = util.getStorageData(globalData.localJsonName);
    keyService.editCategory(currCategory, localJsonData);
    this.saveCategory(localJsonData, true);
  },
  sortBind: function(event) {
    if (globalData.password == '') {
      util.showToast('必须输入主密码才能操作！');
      return;
    }

    const value = parseInt(event.currentTarget.dataset.value);
    const currCategory = event.currentTarget.dataset.catitem;
    const catArry = this.data.catArry;

    let minSort = 1;
    let maxSort = 1;
    let nextCategory = undefined;
    for (let i = 0; i < catArry.length; i++) {
      if (catArry[i].sort > maxSort)
        maxSort = catArry[i].sort;
      if (catArry[i].sort < minSort)
        minSort = catArry[i].sort;
      if (catArry[i].sort == currCategory.sort + value)
        nextCategory = catArry[i];
    }

    if (!nextCategory)
      return;
    currCategory.name = cryptoJs.AesEncrypt(currCategory.name, globalData.password);
    currCategory.sort = currCategory.sort + value;

    nextCategory.name = cryptoJs.AesEncrypt(nextCategory.name, globalData.password);
    nextCategory.sort = nextCategory.sort - value;

    let localJsonData = util.getStorageData(globalData.localJsonName);
    keyService.editCategory(currCategory, localJsonData);
    keyService.editCategory(nextCategory, localJsonData);
    this.saveCategory(localJsonData);
  },
  saveCategory: function(localJsonData, isShowToast = false) {
    const editSucces = () => {
      let catArry = keyService.deCategoryArry(localJsonData.category, globalData.password); //解密数据
      this.setData({
        editVisible: false,
        autofocus: false,
        currCategory: {},
        catArry: catArry
      })
      isShowToast && util.showToast('修改成功！');
    };
    const editFail = () => {
      this.setData({
        editVisible: false,
        autofocus: false
      })
      isShowToast && util.showToast('修改失败！');
    };

    localJsonData.time = new Date().getTime();
    util.setStorageData(globalData.localJsonName, localJsonData)

    if (!globalData.userInfo.isCloudSyn) {
      editSucces();
      return;
    }
    util.setCloudDbData(globalData.cloudJsonName, localJsonData).then(res => {
      editSucces();
    }).catch(err => {
      console.error(err);
      editFail();
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const localJsonData = util.getStorageData(globalData.localJsonName);
    const catArry = keyService.deCategoryArry(localJsonData.category, globalData.password); //解密数据

    this.setData({
      catArry: catArry
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  }
})