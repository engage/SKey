Component({
  externalClasses: ['h-class', 'h-status-class', 'h-title-class'],
  // options: {
  //   multipleSlots: true // 在组件定义时的选项中启用多slot支持
  // },
  properties: {
    statusHeight: {
      type: Number,
      value: 0
    },
    navHeight: {
      type: Number,
      value: 0
    },
    titleBarHeight: {
      type: Number,
      value: 0
    }
  }
})